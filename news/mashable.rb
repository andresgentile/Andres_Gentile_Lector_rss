

require 'rest-client'
require 'json'
require 'colorize'


class Mashable
  attr_accessor :llamar, :traductor
  def initialize
    @llamar = RestClient.get('http://mashable.com/stories.json')
    @traductor = JSON.parse(@llamar.body)
    mostrar
  end

  def mostrar

    @plom = []
    @traductor.each do |index, value|
      if index != 'channel'
        @traductor["#{index}"].each do |value|
          fecha = Date.parse(value['post_date'])
          wem = {"title" => "#{value['title']}",
           "author" => "#{value['author']}",
            "date" => " #{fecha.mday} / #{fecha.mon} / #{fecha.year}",
             "link" => "#{value['link']}"}
          @plom << wem
        end
      end
    end

  end

  def ord
    @plom.sort_by! { |t| t['date'] }
  end
end



#cl = Mashable.new

#puts cl.ord

