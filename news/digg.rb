require 'rest-client'
require 'json'
require 'colorize'

class Digg
  attr_accessor :llamar
  def initialize
    @llamar = RestClient.get('http://digg.com/api/news/popular.json')
    @traductor = JSON.parse(@llamar)
    mostrar
  end

  def mostrar
    @plod = []
    @traductor['data']['feed'].each do |key|


      fecha = Time.at(key['date'].to_i).strftime("%d/%m/%Y")


      wed = {"title" => "#{key['content']['title_alt']}",
       "author" => "#{key['content']['author']}",
        "date" => " #{fecha}",
         "link" => "#{key['content']['original_url']}"}

      @plod << wed
    end
  end

  def ord
    @plod.sort_by! { |t| t['date'] }
  end

end

#cl = Digg.new

#puts cl.ord

